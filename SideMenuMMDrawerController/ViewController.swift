//
//  ViewController.swift
//  SideMenuMMDrawerController
//
//  Created by BS23 on 6/20/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import MMDrawerController


class ViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func LeftSideMenuButtonAction(_ sender: Any)
    {
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }

    
    @IBAction func RightSideMenuButtonAction(_ sender: Any)
    {
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(MMDrawerSide.right, animated: true, completion: nil)
    }
    
}

